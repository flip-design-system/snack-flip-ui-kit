import React, {useState} from 'react';
import {StyleSheet, View, ScrollView, Platform} from 'react-native';
import {useFonts} from 'expo-font';

import {
  DefaultCheckbox,
  RadioButton,
  DashedLine,
  Line,
  FieldDropdown,
  CardKYC,
  FlipColors,
  Text,
  Button,
  FlipGlobe,
  SendMoney,
} from '@flip.id/ui-kit-v2';

const isIOS = Platform.OS === 'ios';

const dataActive = [
  {
    key: 'opt1',
    text: 'Option 1',
  },
  {
    key: 'opt2',
    text: 'Option 2',
  },
];

const dataDisabled = [
  {
    key: 'opt3',
    text: 'Option 3',
  },
  {
    key: 'opt4',
    text: 'Option 4',
  },
];

export default function App() {
  const [isActive, setIsActive] = useState(false);
  const [opt1, setOpt1] = useState(false);
  const [isLoaded] = useFonts({
    'ProximaNova-Bold': require('./assets/fonts/ProximaNova-Bold.otf'),
    [isIOS
      ? 'ProximaNova-BoldIt'
      : 'ProximaNova-Bold-It']: require('./assets/fonts/ProximaNova-Bold-It.otf'),
    'ProximaNova-Medium': require('./assets/fonts/ProximaNova-Medium.otf'),
    [isIOS
      ? 'ProximaNova-MediumIt'
      : 'ProximaNova-Medium-It']: require('./assets/fonts/ProximaNova-Medium-It.otf'),
  });

  if (!isLoaded) {
    return <></>;
  }

  return (
    <ScrollView style={styles.scrollContainer}>
      <Text textType="bold" style={styles.demoTitle}>
        UI KIT V2
      </Text>
      <Text textType="boldItalic" style={styles.demoTitle}>
        by M. Fachrur Ridwan
      </Text>
      <View style={styles.container}>
        <Text style={styles.textTitle}>Radio Button</Text>
        <RadioButton data={dataActive} disabled={false} />
        <RadioButton data={dataDisabled} disabled={true} />
      </View>
      <DashedLine
        dashLength={5}
        dashThickness={1}
        dashColor={FlipColors.grey.lightGrey}
        style={styles.dash}
      />
      <View style={styles.container}>
        <Text style={styles.textTitle}>Checkbox</Text>
        <DefaultCheckbox
          isActive={isActive}
          setIsActive={setIsActive}
          title="Checkbox 1"
          disabled={false}
        />
        <DefaultCheckbox
          isActive={opt1}
          setIsActive={setOpt1}
          title="Checkbox 2"
          disabled={false}
        />
        <DefaultCheckbox title="Checkbox 3" disabled={true} />
      </View>
      <View style={styles.container}>
        <Text style={styles.textTitle}>Dividers</Text>
        <DashedLine
          dashLength={5}
          dashThickness={1}
          dashColor={FlipColors.grey.lightGrey}
          style={styles.dash}
        />
        <Line
          borderColor={FlipColors.neutral.customBlack30Percent}
          style={styles.line}
        />
      </View>
      <View style={styles.container}>
        <Text style={styles.textTitle}>Dropdown</Text>
        <View style={{marginTop: 15}}>
          <FieldDropdown
            placeholder="contoh dropdown 1"
            // sourceIcon={Icons.defaultIcon}
          />
        </View>
        <View style={{marginTop: 15}}>
          <FieldDropdown placeholder="contoh dropdown 2" />
        </View>
        <View style={styles.container}>
          <Text style={styles.textTitle}>KYC Process Card</Text>
          <CardKYC
            isPoint={51}
            isFailedOrRejectedKYC={false}
            isPendingKYC={false}
            isIdentityNumberFilled={false}
            isNewlyCreatedAccount={true}
          />
          <CardKYC
            isPoint={50}
            isFailedOrRejectedKYC={true}
            isPendingKYC={true}
            isIdentityNumberFilled={false}
            isNewlyCreatedAccount={false}
          />
        </View>
        <View style={styles.container}>
          <Text style={styles.textTitle}>Button Test</Text>
          <Button variant="primary" title="Click Here" style={styles.mTop} />
          <Button variant="secondary" title="Click Here" style={styles.mTop} />
        </View>
        <View style={styles.container}>
          <Text style={styles.textTitle}>Icons Test</Text>
          <FlipGlobe/>
          <SendMoney/>
        </View>
      </View>
    </ScrollView>
  );
}

const styles = StyleSheet.create({
  scrollContainer: {
    marginTop: 55,
  },
  demoTitle: {
    textAlign: 'center',
    paddingVertical: 10,
  },
  container: {
    justifyContent: 'center',
    alignContent: 'center',
    display: 'flex',
    padding: 25,
    marginTop: 20,
  },
  textTitle: {
    marginBottom: 15,
    fontSize: 20,
    textAlign: 'center',
  },
  dash: {
    marginHorizontal: 15,
  },
  line: {
    marginHorizontal: 15,
  },
  mTop: {
    marginTop: 10,
  },
});
